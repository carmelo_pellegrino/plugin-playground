#include <iostream>
#include <dlfcn.h>
#include <cstdlib>

typedef void (*pf)(void);

int main(int argc, char* argv[])
{
  if (argc != 2) return EXIT_FAILURE;
  void* const plugin = dlopen(argv[1], RTLD_LAZY);

  if (plugin) {
     pf f = reinterpret_cast<pf>(dlsym(plugin, "func"));
     f();
     dlclose(plugin);
  } else {
    std::cout << dlerror() << '\n';
  }
}
