# Plugin playground

## Build
```
#!shell
# Compile the main executable

g++ -ldl -o main main.cpp

# Compile the shared library
g++ -fpic -c lib.cpp
g++ -shared -o libfunc.so lib.o
```

# Execute

```
#!shell
./main libfunc.so
```
